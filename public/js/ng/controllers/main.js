/**
 * Created by anique on 9/25/14.
 */
var mainControllers = angular.module('mainControllers',[]);

mainControllers.controller('searchController',[
    '$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
        $scope.searchTerm = $routeParams.searchTerm;

    }
]);
mainControllers.controller('indexController',[
    '$scope', '$http', function ($scope,$http) {
         $http.get('/topics/index.json').success(
             function (data) {
                $scope.topics=data;
             }
         );
    }
]);

