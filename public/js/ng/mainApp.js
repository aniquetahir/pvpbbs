/**
 * Created by anique on 9/25/14.
 */
var mainApp = angular.module('mainApp',['ngRoute','mainControllers']);

var viewBase = '/js/ng/views/main/';
mainApp.config([
    '$routeProvider',function ($routeProvider) {
        $routeProvider.when('/',{
            templateUrl: viewBase + 'index.html',
            controller: 'indexController'
        })
        .when('/search/:searchTerm', {
            templateUrl: viewBase + 'search.html',
            controller: 'searchController'
        })
        .when('/topic/:id', {
            templateUrl: viewBase + 'topic.html',
            controller: 'topicController'
        });
    }
]);